package com.sudespacho.webdriver.tests;

import com.acenhauer.corball.selenium.BaseWebDriver;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by guillemhs on 2015-11-16.
 */
public class WebDriverTest extends BaseWebDriver {
    /**
     * please run this test to make sure environment has been setup correctly
     */
    @Test
    public void webDriverTestDemo() {
        driver().get("http://www.iberley.es");
        driver().findElement(By.id("form_term")).clear();
        driver().findElement(By.id("form_term")).sendKeys("bitcoin");
        driver().findElement(By.id("form_save")).click();
    }
}
